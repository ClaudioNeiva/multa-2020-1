package br.ucsal.testequalidade20162.business;

import java.util.ArrayList;

import br.ucsal.testequalidade20162.domain.Condutor;
import br.ucsal.testequalidade20162.domain.Multa;
import br.ucsal.testequalidade20162.domain.TipoMultaEnum;
import br.ucsal.testequalidade20162.exception.RegistroDuplicadoException;
import br.ucsal.testequalidade20162.exception.RegistroNaoEncontradoException;
import br.ucsal.testequalidade20162.persistence.CondutorDao;

public class CondutorBo {

	public static void inserir(Integer numeroCnh, String nome) throws RegistroDuplicadoException {
		try {
			CondutorDao.encontrarByNumeroCnh(numeroCnh);
			throw new RegistroDuplicadoException();
		} catch (RegistroNaoEncontradoException e) {
			Condutor condutor = new Condutor(numeroCnh, nome);
			CondutorDao.inserir(condutor);
		}
	}

	public static void registrarMulta(Integer numeroCnh, Multa multa)
			throws RegistroNaoEncontradoException, RegistroDuplicadoException {
		Condutor condutor = CondutorDao.encontrarByNumeroCnh(numeroCnh);
		condutor.incluirMulta(multa);
	}

	public static ArrayList<Multa> pesquisarMultas(Condutor condutor, TipoMultaEnum tipoMulta) {
		ArrayList<Multa> multasPesquisa = new ArrayList<>();
		for (Multa multa : condutor.getMultas()) {
			if (multa.getTipo().equals(tipoMulta)) {
				multasPesquisa.add(multa);
			}
		}
		return multasPesquisa;
	}

	public static Boolean verificarCarteiraSuspensa(Condutor condutor) {
		if (calcularTotalPontos(condutor) >= 21) {
			return true;
		}
		return false;
	}
	
	public static Integer calcularTotalPontos(Condutor condutor) {
		Integer totalPontos = 0;
		for (Multa multa : condutor.getMultas()) {
			totalPontos += multa.getTipo().getPontos();
		}
		return totalPontos;
	}

}
